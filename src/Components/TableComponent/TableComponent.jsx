import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';
import "./TableComponent.css"
import { auto } from '@popperjs/core';




export default function TableComponent({columns , rows}) {


  const rowsWithKeys = rows.map((row, index) => ({id: index, ...row  }));

  return (
    <div style={{ height: 400 , width:auto }}>
      <DataGrid
        rows={rowsWithKeys}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: { page: 0, pageSize: 5 },
          },
        }}
        pageSizeOptions={[5, 10]}
        checkboxSelection
      />
    </div>
  );
}