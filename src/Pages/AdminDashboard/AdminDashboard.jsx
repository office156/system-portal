import React from 'react'
import "./AdminDashboard.css"
// import Sidebar from '../../Components/Sidebar_backup/Sidebar'
import DashboardCards from '../../Components/DashboardCards/DashboardCards'
import DBLineChart from '../../Components/DashBoardLineChart/DBLineChart'
import DBAreaChart from '../../Components/DBAreaChart/DBAreaChart'
import DBUserTable from '../../Components/DashboardUserTable/DBUserTable'
import SideBar from '../../Components/Sidebar/SideBar'

export default function AdminDashboard() {
  return (
    <div className='AdminDashboardTop'>
        {/* <Sidebar className="SidebarAdminPage"></Sidebar> */}
        <SideBar></SideBar>

        <div className="AdminDashboardContentsTop pt-5">
          <DashboardCards></DashboardCards>

          <div className="AdminChartMiddle d-flex justify-content-around py-5">
          <div className="AdminChartsLeft mx-2">
            <DBLineChart></DBLineChart>
          </div>
          <div className="AdminChartsRight mx-2">
            <DBAreaChart></DBAreaChart>
          </div>
        </div>

        <div className="AdminDashboardUserTable mx-2">
          <DBUserTable></DBUserTable>
        </div>

        </div>

    </div>
  )
}
