import React, { useState } from 'react'
import "./RootAccount.css"
import { useEffect } from 'react';
import axios from 'axios';

export default function RootAccountContent() {


    const [loginTime , setLoginTime] = useState();
    const [systemConfo , setSystemConfo] = useState([
        {
          hostname: "something",
          transient: "something",
          iconname: "something",
          chassis: "something",
          machineid: "something",
          bootid: "something",
          operatingsystem: "something",
          cpeosname: "something",
          kernel: "something",
          arch: "something",
        }
      ]);

    useEffect(() => {
        axios.get('http://127.0.0.1:8000/api/get-lastlogin').then((response)=>{
            console.log(response.data);
            setLoginTime(response.data)
          }).catch((error)=>{
            console.log(error)
          })

        axios.get('http://127.0.0.1:8000/api/get-systemConfiguration').then((response)=>{
            console.log(response.data);
            setSystemConfo(response.data)
          }).catch((error)=>{
            console.log(error)
          })
      },[]);
    

    return (

        <div className="RootAccountContent">
            <div className="RootAccountrowTop">
                <div class="container text-start">

                    <div class="row">
                        <div class="col me-2 border py-3 px-3">
                            <h3 className=''>Health</h3>
                            <div className="serviceFailLine">
                            <i class="fa-solid fa-circle-exclamation"></i>
                                <span></span>
                                <span> service has failed</span>
                            </div>
                            <div className="serviceFailLine">
                            <i class="fa-solid fa-circle-check"></i>
                                <span></span>
                                <span> service is running ok</span>
                            </div>
                            <div className="securityUpdatesLine">
                            <i class="fa-solid fa-triangle-exclamation"></i>
                                <span></span>
                                <span> security updates available</span>
                            </div>
                            <div className="lastSuccessfulLoginLine">
                                <i></i>
                                <span>Last successful login : </span>
                                <span></span>
                                <p>{loginTime}</p>
                                <a href="">View login history</a>
                            </div>

                        </div>
                        <div class="col ms-2 border py-3 px-3">
                            <h3 className=''>Usage</h3>
                            <div className="systemInformationTable">
                                <table className='w-100'>
                                    <tr>
                                        <td>CPU</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Memory</td>
                                        <td></td>
                                    </tr>

                                </table>
                            </div>
                            <div className="">
                                <a href="">View hardware details</a>
                            </div>

                        </div>
                    </div>

                    <div class="row mt-4">

                        <div class="col me-2 border py-3 px-3">
                            <h3 className=''>System information</h3>
                            <div className="systemInformationTable">
                                <table className='w-100'>
                                    <tr>
                                        <td>Model</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Asset tag</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Machine ID</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Uptime</td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <div className="">
                                <a href="">View hardware details</a>
                            </div>
                        </div>

                        <div class="col ms-2 border py-3 px-3">
                            <h3 className=''>Configuration</h3>
                            <div className="systemInformationTable">
                                <table className='w-100'>
                                    <tr>
                                        <td>Hostname</td>
                                        <td>{systemConfo[0]["hostname"]}</td>
                                        {/* <td>{systemConfo.length > 0 ? <span></span> : <></>}</td> */}
                                    </tr>
                                    <tr>
                                        <td>System time</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Domain</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Performance profile</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Crypto policy</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Secure shell keys</td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <div className="">
                                <a href="">View hardware details</a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    )
}
