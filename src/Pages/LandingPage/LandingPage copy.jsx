import React from 'react'
import "./LandingPage.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useNavigate } from 'react-router-dom'

export default function LandingPage() {

  const navigate = useNavigate();

  return (
    <div className='landingPageTop'>
      <div className="top landingPageTopSection d-flex flex-column align-items-center">
        <br />
        <p className='topParagraph text-center'>
          CDAC cluster management software automate the process of building and managing modern
          <br />
          high-performance Linux clusters, elimating complexity and enabling flexibility.
        </p>

        <div className="buttons-top">
          <button className='landingPageTopButtons btnAdmin'>ADMIN MANUAL</button>
          <button className='landingPageTopButtons'>USER MANUAL</button>
        </div>

      </div>
      <div className="middle landingPageMiddleSection container">
        <div className="row d-flex justify-content-around middleRow">
          <div className="col-3 cardColumns">
            <div class="card cardStyling">
              <img src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg" class="card-img-top cardImageTop" alt="..."></img>
              
              <div class="card-body cardBody">
              <a  class="buttonMiddle">
                  <i class="fa-solid fa-link"></i>
                </a>
                <h5 class="card-title">CDAC View</h5>
                <p class="card-text cardSubtext">The web application front end to cluster management in Bright Cluster Manager</p>

              </div>
            </div>
          </div>
          <div className="col-3 cardColumns">
            <div class="card cardStyling">
              <img src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg" class="card-img-top cardImageTop" alt="..."></img>
              <div class="card-body cardBody">
              <a  class="buttonMiddle">
                  <i class="fa-solid fa-link"></i>
                </a>
                <h5 class="card-title">User Portal</h5>
                <p class="card-text cardSubtext">View the state of the cluster. This interface presents data about the system.</p>

              </div>
            </div>
          </div>
          <div className="col-3 cardColumns">
            <div class="card cardStyling">
              <img src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg" class="card-img-top cardImageTop" alt="..."></img>
              <div class="card-body cardBody">
              <a onClick={()=>{
                navigate("/login")
              }} class="buttonMiddle">
                  <i class="fa-solid fa-link"></i>
                </a>
                <h5 class="card-title">Ceph Dashboard</h5>
                <p class="card-text cardSubtext">Object, block, and file system storage in a single unified storage cluster.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bottom landingPageBottomSection">
        <p>2009-2020 CDAC Computing, Inc. All rights reserved.</p>
      </div>
    </div>
  )
}
