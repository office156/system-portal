import React from 'react'
import SideBar from '../../Components/Sidebar/SideBar'
import RootAccountContent from './RootAccountContent'

export default function RootAccount() {
  return (
    <div>
      <SideBar childComponent={<RootAccountContent />} ></SideBar>


    </div>
  )
}
