import React, { useState } from 'react';
import "./MultiPage.css"

function MultiPageUserForm() {
  const [currentPage, setCurrentPage] = useState(1);
  const [formData, setFormData] = useState({
    name: '',
    id: '',
    commonName: '',
    surname: '',
    groupId: '',
    loginShell: '',
    homeDirectory: '',
    password: '',
    confirmPassword: '',
    email: '',
    profile: ''
  });

  const goToNextPage = () => {
    setCurrentPage(currentPage + 1);
  };

  const goToPrevPage = () => {
    setCurrentPage(currentPage - 1);
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  const clearPage = (event) => {
    setFormData({
      name: '',
      id: '',
      commonName: '',
      surname: '',
      groupId: '',
      loginShell: '',
      homeDirectory: '',
      password: '',
      confirmPassword: '',
      email: '',
      profile: ''
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(formData);
    setFormData({
      name: '',
      id: '',
      commonName: '',
      surname: '',
      groupId: '',
      loginShell: '',
      homeDirectory: '',
      password: '',
      confirmPassword: '',
      email: '',
      profile: ''
    });
  };

  return (
    <div className='h-100'>
      <form className='h-100 AdminUserAddFormMain' onSubmit={handleSubmit}>
        {currentPage === 1 && (
          <div className='h-100 d-flex flex-column justify-content-around px-3 text-start AdminUserAddDivs'>

            <div className="row">
              <div className="col">
                <input type="text" placeholder='Name' name="name" value={formData.name} onChange={handleInputChange} />


              </div>
              <div className="col">

                <input type="text" placeholder='ID' name="id" value={formData.id} onChange={handleInputChange} />

              </div>
            </div>

            <div className="row">
              <div className="col">
                <input type="text" placeholder='Common name' name="commonName" value={formData.commonName} onChange={handleInputChange} />

              </div>
              <div className="col">


                <input type="text" placeholder='Surname' name="surname" value={formData.surname} onChange={handleInputChange} />

              </div>
            </div>





            <input type="text" placeholder='Group ID' name="groupId" value={formData.groupId} onChange={handleInputChange} />

            <div className="AllAdminAddUserFormButtons d-flex justify-content-between">
              <button type="button" onClick={goToNextPage}>

                <span className='me-2'>Next</span>
                <i class="fa-solid fa-arrow-right"></i>
              </button>
              <button type="button" onClick={clearPage}>
                <i class="fa-solid fa-trash"></i>
                <span className='ms-2'>Delete</span>
              </button>
            </div>
          </div>
        )}

        {currentPage === 2 && (
          <div className='h-100 w-100 d-flex flex-column justify-content-around px-3 text-start AdminUserAddDivs'>

            <div className="row">
              <div className="col">
                <input type="text" placeholder='Login shell' name="loginShell" value={formData.loginShell} onChange={handleInputChange} />

              </div>
              <div className="col">

                <input type="text" placeholder='Home directory' name="homeDirectory" value={formData.homeDirectory} onChange={handleInputChange} />

              </div>
            </div>
            <div className="row">
              <div className="col d-flex">

                <input type="text" placeholder='Password' name="password" value={formData.password} onChange={handleInputChange} />
                <input type="text" placeholder='Confirm password' name="confirmPassword" value={formData.confirmPassword} onChange={handleInputChange} />

              </div>
              <div className="col"></div>
            </div>

            <div className="row">
              <div className="col">
                <input type="email" placeholder='Email' name="email" value={formData.email} onChange={handleInputChange} />

              </div>
              <div className="col"></div>
            </div>

            <div className="row">
              <div className="col">

                <input type="text" placeholder='Profile' name="profile" value={formData.profile} onChange={handleInputChange} />

              </div>
              <div className="col"></div>
            </div>



            <div className="AllAdminAddUserFormButtons d-flex justify-content-between">
              <button type="button" onClick={goToPrevPage}>
              <i class="fa-solid fa-arrow-left"></i>
              <span className='ms-2'>Back</span>
              </button>

              <div>
                <button className='ms-2' type="button" onClick={clearPage}>
                  <i class="fa-solid fa-arrow-rotate-left"></i>
                  <span className='ms-2'>Revert</span>
                </button>
                <button className='ms-2' type="button" onClick={clearPage}>
                  <i class="fa-solid fa-trash"></i>
                  <span className='ms-2'>Delete</span>
                </button>
                <button className='ms-2 specialButton' type="submit">
                  <i class="fa-solid fa-floppy-disk"></i>
                  <span className='ms-2 '>Save</span>
                </button>
              </div>
            </div>
          </div>
        )}
      </form>
    </div>
  );
}

export default MultiPageUserForm;
