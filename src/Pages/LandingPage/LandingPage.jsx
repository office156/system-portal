import React from 'react'
import "./LandingPage.css"
import { useNavigate } from 'react-router-dom'


export default function LandingPage() {

  const navigate = useNavigate();

  return (
    <div className='landingPageTop'>

      <div className="leftSideLandingPage">

      <div className='landingPageSidebar pt-5 border-end border-dark-subtle'>
        CDAC cluster management software automate the process of building and managing modern
    high-performance Linux clusters, elimating complexity and enabling flexibility.
    </div>

      </div>

      <div className="rightSideLandingPage p-5 d-flex flex-column justify-content-between align-items-end">

        <div className="rigthSideTop">

        <div class="card landingPageCard">
  <img src="https://media.istockphoto.com/id/1093110112/photo/picturesque-morning-in-plitvice-national-park-colorful-spring-scene-of-green-forest-with-pure.jpg?s=612x612&w=0&k=20&c=lpQ1sQI49bYbTp9WQ_EfVltAqSP1DXg0Ia7APTjjxz4=" class="card-img-top d-sm-none d-md-block" alt="..."></img>
  <div class="card-body text-start">
    <h5 class="card-title">Ceph Dashboard</h5>
    <p class="card-text">Object, block, and file system storage in a single unified storage cluster.</p>
    <button className='landingPageTopButtons' onClick={()=>{
      navigate("login")
    }}>Login</button>
  </div>
</div>

        </div>

        <div className="rigthSideBottom mt-3">

        <div className="">

        <div className="">
          <button className='landingPageTopButtons btnAdmin me-2'>ADMIN MANUAL</button>
          <button className='landingPageTopButtons me-2'>USER MANUAL</button>
        </div>

      </div>

        </div>

      </div>

    </div>
  )
}
