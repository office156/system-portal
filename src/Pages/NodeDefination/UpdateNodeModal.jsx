import React from 'react'
import { useState } from 'react';
import axios from 'axios';
import { ToastContainer , toast } from 'react-toastify';
import { useEffect } from 'react';

export default function UpdateNodeModal({setData,node}) {
    
    const [hostname , sethostname] = useState();
    const [ip , setip] = useState();
    const [groups , setgroups] = useState();
    const [managementIp , setmanagementIp] = useState("");
    const [ManagementUsername , setManagementUsername] = useState();
    const [MacAddress , setMacAddress] = useState();
    const [password , setpassword] = useState("");
    // const [enter , setenter] = useState(node[0][0]"");
    
    // const [formData, setFormData] = useState({
    //     email: '',
    //     ip: '',
    //     groups: '',
    //     managementIp: '',
    //     ManagementUsername: '',
    //     MacAddress: '',
    //     password: '',
    //   });

      // const getFormData = ()=>{
      //   const url = "http://127.0.0.1:8000/api/admin/get-node-info/" + id 
      //   axios.get(url).then((response)=>{
      //       // console.log(response.data[0][0])
      //       sethostname(response.data[0][0])
      //       setip(response.data[0][3])
      //       setmanagementIp(response.data[0][6])
      //       setenter("enter")
      //       // setFormData({ ip:  });
      //       // setFormData({ groups: response.data[0][1] });
      //       // setFormData({ managementIp:  });
      //       // setFormData({ ManagementUsername: response.data[0][0] });
      //       // setFormData({ MacAddress: response.data[0][9] });
      //       // setFormData({ password: response.data[0][0] });
      //   }).catch((error)=>{
      //       console.log(error)
      //   })
      // }

      useEffect(() => {
        // Your effect logic here
        console.log('Child effect executed');
        console.log(node)
        sethostname(node[0][0])
        setip(node[0][2])
        setgroups(node[0][1])
        setManagementUsername(node[0][3])
        setMacAddress(node[0][5])
        setpassword("")
        setmanagementIp("")
      },[node]);
    
      const handleSubmitNode = async (event) => {
        event.preventDefault();
        if (hostname == "" || ip == "" || groups == "" || managementIp == "" || ManagementUsername == "" || password == "" || MacAddress == "") {
          // some input fields are empty, show an error message
            console.log('Please fill all input fields.');
          toast.error("Please enter all the fields")
  
  
        } else {
  
                  // all input fields are filled, do something
                  console.log('All input fields are filled.');
                  // console.log(isIP(ip))
                  // console.log(isIP(managementIp))
                    // console.log(formData);
                    try {
                      const response = await axios.post('http://127.0.0.1:8000/api/admin/update-node-defination', {
                        hostname,
                        ip,
                        MacAddress,
                        managementIp,
                        ManagementUsername,
                        password,
                        groups
                      });
                      console.log(response);
                    } catch (error) {
                      console.error(error);
                      toast.error(error)
                    }
  
  
  
  
                    // setFormData({
                    //   email: '',
                    //   ip: '',
                    //   groups: '',
                    //   managementIp: '',
                    //   ManagementUsername: '',
                    //   MacAddress: '',
                    //   password: '',
                    // });
  
                    toast.success("Node updated successfully");
        }
  
        //Setting new data
  
        axios.get('http://127.0.0.1:8000/api/test').then((response)=>{
          console.log(response.data);
          setData(response.data)
        }).catch((error)=>{
          console.log(error)
        })
        
  
      };

  return (
    
    <>

{node.length > 0 ? (
  <div>
  <div class="modal-body">
      <form>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Hostname</label> 
    <input type="text" class="form-control"  name="email" disabled value={hostname} onChange={(event)=>sethostname(event.target.value)}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">IP Address</label>
    <input type="text" class="form-control"  name="ip" value={ip} onChange={(event)=>setip(event.target.value)}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Groups</label>
    <input type="text" class="form-control"  name="groups" value={groups} onChange={(event)=>setgroups(event.target.value)}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Management IP</label>
    <input type="text" class="form-control"  name="managementIp" value={managementIp} onChange={(event)=>setmanagementIp(event.target.value)}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Management Username</label>
    <input type="text" class="form-control" name="ManagementUsername" value={ManagementUsername} onChange={(event)=>setManagementUsername(event.target.value)}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">MAC address</label>
    <input type="text" class="form-control" name="MacAddress" value={MacAddress} onChange={(event)=>setMacAddress(event.target.value)}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="exampleInputPassword1" class="text-start">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" name="password" value={password} onChange={(event)=>setpassword(event.target.value)}></input>
  </div>

</form>


      </div>
      <div class="modal-footer">
        <button type="button" class="NodeDefinationButtons button-55" data-bs-dismiss="modal">Close</button>
        <button type="button" class="NodeDefinationButtons button-55" onClick={handleSubmitNode}>Save changes</button>
      </div>
      </div>

      ) : (
        <p>
          We currently don't have information on this node.
        </p>
      )}

</>


  )
}
