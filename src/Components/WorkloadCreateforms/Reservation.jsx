import React from 'react'

export default function Reservation() {
  return (
    <div>
                <div className="batchjobModal">
<button type="button" class="" data-bs-toggle="modal" data-bs-target="#ReservationModal">
Reservation
</button>

<div class="modal fade" id="ReservationModal" tabindex="-1" aria-labelledby="ReservationModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="ReservationModalLabel">Reservation</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div className="formInputs">
        <label htmlFor="" className="form-label">Accounts</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">BurstBuffer</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Core_Count</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Duration</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Features</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Flags</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">MaxStartDelay</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Name</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Node_Count</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Node_list</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Partition</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Time_End</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Time_Start</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">TRES</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Users</label>
        <input type="text" className="form-control" />
        </div>
        <div className="formInputs">
        <label htmlFor="" className="form-label">Watts</label>
        <input type="text" className="form-control" />
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="" data-bs-dismiss="modal">Close</button>
        <button type="button" class="">Save changes</button>
      </div>
    </div>
  </div>
</div>
        </div>
    </div>
  )
}
