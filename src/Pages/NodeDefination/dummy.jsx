import * as React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import axios from 'axios';
import { toast } from 'react-toastify';
import { useEffect } from 'react';
import { FormControl , InputLabel , Select , MenuItem } from '@mui/material';
import { useState } from 'react';
import TestModal from './TestModal';
import UpdateNodeModal from './UpdateNodeModal';


function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// Since 2020 all major browsers ensure sort stability with Array.prototype.sort().
// stableSort() brings sort stability to non-modern browsers (notably IE11). If you
// only support modern browsers you can replace stableSort(exampleArray, exampleComparator)
// with exampleArray.slice().sort(exampleComparator)
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}




function EnhancedTableHead(props) {
  const { groups, groupsfunc, onSelectAllClick, order, orderBy, editselected, numSelected, rowCount, onRequestSort, selectedItems , onButtonClick} =
    props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };
  const [age, setAge] = React.useState('all');
  const [grouplist , setgrouplist] = React.useState([]);
  const [selectedGroup , setSelectedGroup] = useState();

  const handleChange = (event) => {
    setAge(event.target.value);
    handleSelect(event.target.value);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('http://127.0.0.1:8000/api/node_groups_list');
      setgrouplist(response.data);
      // console.log(response.data)
      // setgroups(response.data[0].groups)
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const handleSelect = async (selectedGroup) => {
    try {
      const url = "http://127.0.0.1:8000/api/node_list_group_filter/" + selectedGroup
      const response = await axios.get(url);
      onButtonClick(response.data)
      console.log(response.data)
      // setgroups(response.data[0].groups)
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  // if (!Array.isArray(groups)) {
  //   return <div>Error: groups is not an array</div>;
  // }

  const deleteNodes = (nodes) =>{
    console.log(nodes)
    {nodes.length > 0 ? (
      axios.delete('http://127.0.0.1:8000/api/admin/dm-nodes',{data:{nodes}}).then((response)=>{
      console.log(response);
      toast.success("Nodes deleted successfully")
      editselected([])
      //Reset the nodelist
      axios.get('http://127.0.0.1:8000/api/test').then((response)=>{
        console.log(response.data);
        onButtonClick(response.data)
      }).catch((error)=>{
        console.log(error)
      })
      // setData(response.data)
    }).catch((error)=>{
      console.log(error)
      toast.error("Server error")
    })
    // axios.delete("")
    ) : (
      toast.error("Please select at least one node.")
    )}

  }

  // const renderedGroups = groups.map((group) => (
  //   <div key={group}>{group}</div>
  // ));

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            color="primary"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              'aria-label': 'select all desserts',
            }}
          />
        </TableCell>
                      <TableCell>Node</TableCell>
              <TableCell>
              <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
      <InputLabel id="demo-select-small-label">Groups</InputLabel>
      <Select
        labelId="demo-select-small-label"
        id="demo-select-small"
        label="Groups"
        value={age}
        onChange={handleChange}
      >
        <MenuItem key="all" value="all">All</MenuItem>
        {grouplist.map((item) => (
          // <option key={item["groups"]} value={item}>{item}</option>
          <MenuItem key={item[0]} value={item[0]} >{item[0]}</MenuItem>
        ))}
        {/* <MenuItem value={10}>Ten</MenuItem>
        <MenuItem value={20}>Twenty</MenuItem>
        <MenuItem value={30}>Thirty</MenuItem> */}
      </Select>
    </FormControl>
          {/* <select>
        {grouplist.map((item) => (
          <option key={item["groups"]} value={item}>{item}</option>
        ))}
      </select> */}
              </TableCell>
              <TableCell>Status</TableCell>
              <TableCell>OS</TableCell>
              <TableCell>IP</TableCell>
              <TableCell>Info</TableCell>
                                  <TableCell>
                      <button className='border-0 bg-none'>
                        <i class="fa-solid fa-pen"></i>
                      </button>
                    </TableCell>
                                  <TableCell>
                      <button className='border-0 bg-none' onClick={()=>deleteNodes(selectedItems)}>
                        <i class="fa-solid fa-trash"></i>
                      </button>
                    </TableCell>

      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};



export default function EnhancedTable({rows , onButtonClick ,grouplist ,grouplistfunction}) {
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [nodeInfo , setNodeInfo] = useState([]);
  const [updateNodeInfo , setupdateNodeInfo] = useState([]);
  // const [hostname , sethostname] = useState();
  // const [ip , setip] = useState();
  // const [groups , setgroups] = useState();
  // const [managementIp , setmanagementIp] = useState("");
  // const [ManagementUsername , setManagementUsername] = useState();
  // const [MacAddress , setMacAddress] = useState();
  // const [password , setpassword] = useState("");
  const [selectedNode , setselectedNode] = useState("");

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelected = rows.map((n) => n.node);
      setSelected(newSelected);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  // const deleteNode = (node) => {
  //   // setPage(newPage);
  //   // console.log(node)
    
  // };

  const deleteNode = (node) =>{

    axios.delete('http://127.0.0.1:8000/api/admin/ds-node',{data:{node}}).then((response)=>{
      console.log(response);
      toast.success("Node deleted successfully")
  
      //Reset the nodelist
      axios.get('http://127.0.0.1:8000/api/test').then((response)=>{
        console.log(response.data);
        onButtonClick(response.data)
      }).catch((error)=>{
        console.log(error)
      })
      // setData(response.data)
    }).catch((error)=>{
      console.log(error)
      toast.error("Server error")
    })
    // axios.delete("")
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };

  const getNodeInfo = (node) =>{
    // const url = "http://127.0.0.1:8000/api/testing"
    const url = "http://127.0.0.1:8000/api/admin/get-node-info/" + node 
    axios.get(url).then((response)=>{
      console.log(response.data)
      setNodeInfo(response.data)
      setselectedNode(node)
    }).catch((error)=>{
      console.log(error)
    })
  }

  const getUpdateNodeInfo = (node) =>{
    // const url = "http://127.0.0.1:8000/api/testing"
    const url = "http://127.0.0.1:8000/api/admin/get-update-node-info/" + node 
    axios.get(url).then((response)=>{
      console.log(response.data[0][0])
      setupdateNodeInfo(response.data)
      // console.log(updateNodeInfo)
      // setselectedNode(node)
      // console.log(updateNodeInfo)
    }).catch((error)=>{
      console.log(error)
    })
  }


  const isSelected = (name) => selected.indexOf(name) !== -1;

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  return (
    <Box sx={{ }}>
      <Paper sx={{  mb: 2 }}>
        {/* <EnhancedTableToolbar numSelected={selected.length} /> */}
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader>
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
              selectedItems={selected}
              editselected={setSelected}
              data={rows}
              onButtonClick={onButtonClick}
              groups={grouplist}
              groupsfunc={grouplistfunction}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row[0]);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row[0]}
                      selected={isItemSelected}
                      sx={{ cursor: 'pointer' }}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          color="primary"
                          onClick={(event) => handleClick(event, row[0])}
                          checked={isItemSelected}
                          inputProps={{
                            'aria-labelledby': labelId,
                          }}
                        />
                      </TableCell>
<TableCell>{row[0]}</TableCell>
                    <TableCell>{row[1]}</TableCell>
                    <TableCell>
                    {row[2] !== null ? (
                      <>{row[2]}</>
        
      ) : (
        <>NULL</>
      )}
                    </TableCell>
                    <TableCell>
                    {row[4] !== null ? (
                      <>{row[4]}</>
        
      ) : (
        <>NULL</>
      )}
                    </TableCell>
                    <TableCell>{row[3]}</TableCell>
                    <TableCell>
                      <button type="button" class="" data-bs-toggle="modal" data-bs-target="#exampleModal" onClick={()=>{
                        console.log("testing")
                        getNodeInfo(row[0])
                      }}>
                      <i class="fa-solid fa-circle-info"></i>
                      </button>
                    </TableCell>
                    <TableCell>
                      {/* <UpdateNodeModal setData={onButtonClick} id={row[0]}></UpdateNodeModal> */}
                      <button type="button" class="" data-bs-toggle="modal" data-bs-target="#UpdateNodeModal" onClick={()=>{
                        console.log("in update modal button")
                        getUpdateNodeInfo(row[0])
                      }}>
                      <i class="fa-solid fa-pen"></i>
                      </button>
                    </TableCell>
                    <TableCell>
                      <button onClick={()=>{
                        deleteNode(row[0])
                      }}>
                        <i class="fa-solid fa-trash"></i>
                      </button>
                    </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
      {/* <!-- Modal --> */}
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Node Info</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <TestModal node={selectedNode} nodeInfo={nodeInfo}></TestModal>
      </div>
      <div class="modal-footer">
        <button type="button" class="" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="UpdateNodeModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Node Info</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        
        {updateNodeInfo.length > 0 ? (
        <UpdateNodeModal setData={onButtonClick} node={updateNodeInfo}  ></UpdateNodeModal>
      ) : (
        <p>No info found.</p>
      )}
        
      </div>
    </div>
  </div>
</div>


    </Box>
  );
}