import React from 'react'
import Sidebar from '../../Components/Sidebar_backup/Sidebar'
import DBUserTable from '../../Components/DashboardUserTable/DBUserTable'
import { useState } from 'react';
import { toast , ToastContainer} from 'react-toastify';
import {isIP, isIPv4} from 'is-ip';
import "./WorkloadManager.css"
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import NodeListTable from '../../Components/NodeListTable/NodeListTable';
import { useEffect } from 'react';
import EnhancedTable from './dummy';
import { useRef } from 'react';
import { CircularProgress, LinearProgress } from '@mui/material';
import TableComponent from '../../Components/TableComponent/TableComponent';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import BatchJob from '../../Components/WorkloadCreateforms/BatchJob';
import Partition from '../../Components/WorkloadCreateforms/Partition';
import Reservation from '../../Components/WorkloadCreateforms/Reservation';

import WorkloadSearchTab from '../../Components/WorkloadSearchTab/WorkloadSearchTab';
import SideBar from '../../Components/Sidebar/SideBar';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function WorkloadManager() {

    const [isAddNodeVisible, setIsAddNodeVisible] = useState(false);
    const [isCustomNodeVisible, setIsCustomNodeVisible] = useState(false);
    const [isDeleteNodeVisible, setIsDeleteNodeVisible] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const fileInputRef = useRef(null);
    const [NodeInfo , setNodeInfo] = useState([]);
    const [squeue , setsqueue] = useState([]);
    const [jobsinfo , setjobsinfo] = useState([]);
    const [sinfo , setsinfo] = useState([]);
    const [saccount , setsaccount] = useState([]);

    const [groups , setgroups] = useState([]);
    const [data , setdata] = useState([]);
    const [node , setNode] = useState();
    
    const [selectedFile, setSelectedFile] = useState("");

    const [showqueue , setshowqueue] = useState(false);
    const [showsaccount , setshowsaccount] = useState(false);

    const columns = [
      { field: 'jobId', headerName: 'First name' },
      { field: 'partition', headerName: 'Partition' },
      {
        field: 'name',
        headerName: 'Name'
      },
      {
        field: 'user',
        headerName: 'User'
        // description: 'This column has a value getter and is not sortable.',
        // sortable: false,
        // width: 160,
        // valueGetter: (params) =>
        //   `${params.row.firstName || ''} ${params.row.lastName || ''}`,
      },
      { field: 'st', headerName: 'ST'},
      { field: 'time', headerName: 'Time'},
      { field: 'nodes', headerName: 'Nodes'},
      { field: 'nodelist', headerName: 'Nodelist'}
    ];

    const saccountcolumns = [
      { field: 'account', headerName: 'Account' },
      { field: 'descr', headerName: 'Descr' },
      {
        field: 'org',
        headerName: 'Org'
      }
    ];

    const sinfocolumns = [
      { field: 'reason', headerName: 'Reason' },
      { field: 'user', headerName: 'User' },
      { field: 'timestamp', headerName: 'Timestamp' },
      {
        field: 'nodelist',
        headerName: 'Nodelist'
      }
    ];

    const jobsinfocolumns = [
      { field: 'user', headerName: 'User' },
      { field: 'jobid', headerName: 'jobid' },
      { field: 'jobname', headerName: 'jobname' },
      {
        field: 'partition',
        headerName: 'partition'
      },
      { field: 'state', headerName: 'state' },
      { field: 'timelimit', headerName: 'timelimit' },
      { field: 'start', headerName: 'start' },
      { field: 'end', headerName: 'end' },
      { field: 'elapsed', headerName: 'elapsed' },
      // { field: 'maxrss', headerName: 'maxrss' },
      // { field: 'maxvmsize', headerName: 'maxvmsize' },
      // { field: 'nnodes', headerName: 'nnodes' },
      { field: 'ncpus', headerName: 'ncpus' },
      { field: 'nodelist', headerName: 'nodelist' },
    ];

    
// const rows = [
//   [1, 'Snow','Jon',35 ],
//   [2, 'Lannister','Cersei',42 ],
//   [3, 'Lannister','Jaime',45 ],
//   [4, 'Stark','Arya',16 ],
//   [5, 'Targaryen','Daenerys',null ],
//   [6, 'Melisandre',null,150 ],
//   [7, 'Clifford','Ferrara',44 ],
//   [8, 'Frances','Rossini',36 ],
//   [9, 'Roxie','Harvey',65 ]
// ];

    const fetchSqueue = () =>{
      axios.get('http://127.0.0.1:8000/api/get-squeue').then((response) =>{
        console.log(response)
        setsqueue(response.data)
      }).catch((error) =>{
        console.log(error)
      })
    }

    const fetchjobsInfo = () =>{
      axios.get('http://127.0.0.1:8000/api/get-jobsinfo').then((response) =>{
        console.log(response)
        setjobsinfo(response.data)
      }).catch((error) =>{
        console.log(error)
      })
    }

    const fetchSinfo = () =>{
      axios.get('http://127.0.0.1:8000/api/get-sinfo').then((response) =>{
        console.log(response)
        setsinfo(response.data)
      }).catch((error) =>{
        console.log(error)
      })
    }

    const fetchSaccount = () =>{
      axios.get('http://127.0.0.1:8000/api/get-saccount').then((response) =>{
        console.log(response)
        setsaccount(response.data)
      }).catch((error) =>{
        console.log(error)
      })
    }
    

    const handlequeueClick = () => {
      if (showqueue === false) {
        // If the clicked item is already open, close it
        setshowqueue(true);
        fetchSqueue()
      } else {
        // Open the clicked item
        setshowqueue(false);
      }
    };

    const handlesaccountClick = () => {
      if (showsaccount === false) {
        // If the clicked item is already open, close it
        setshowsaccount(true);
        fetchSaccount()
      } else {
        // Open the clicked item
        setshowsaccount(false);
      }
    };

    const handleFileChange = (event) => {
      const file = event.target.value;
      setSelectedFile(file);
    };


    const searchNode = () => {
      console.log(node)
      axios.get('http://127.0.0.1:8000/api/get-system-node-info',{node}).then((response) =>{
        console.log(response)
        setNodeInfo(response.data)
      }).catch((error) =>{
        console.log(error)
      })
    } 
  
    const handleUpload = () => {
      // You can now access the path of the selected file via selectedFile variable

      if (selectedFile) {
        console.log('File path:', selectedFile);
        setIsLoading(true);
        axios.post('http://127.0.0.1:8000/api/admin/add-image',{selectedFile}).then((response)=>{
          console.log(response)
          toast.success("Image added successfully")
          setIsLoading(false);

          axios.get('http://127.0.0.1:8000/api/admin/images').then((response)=>{
            console.log(response.data.image_list);
            console.log(response)
          //   console.log(response.data.image_list[0]);
            setdata(response.data.image_list)
          }).catch((error)=>{
            console.log(error)
          })

          }).catch((error)=>{
            console.log(error)
          })
        setSelectedFile("");
        // You can send the file path to your backend or perform any other necessary actions
      }else {
        toast.error("please enter something !")
      }
    };

    const [formData, setFormData] = useState({
      email: '',
      ip: '',
      groups: '',
      managementIp: '',
      ManagementUsername: '',
      MacAddress: '',
      password: '',
    });

    const [formData1, setFormData1] = useState({
      multiplegroups: '',
      multiplemanagementIp: '',
      multipleManagementUsername: '',
      multipleMacAddress: '',
      multiplepassword: '',
    });

    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
      console.log(newValue)
      if (newValue == 2) {
        fetchSqueue()
      }
      if (newValue == 3) {
        fetchSaccount()
      }
      if (newValue == 4) {
        fetchSinfo()
      }
      if (newValue == 5) {
        fetchjobsInfo()
      }
      // if (newValue == 4) {
      //   fetchSinfo()
      // }
      setValue(newValue);
    };

    const navigate = useNavigate()

    
    
  useEffect(() => {
    axios.get('http://127.0.0.1:8000/api/admin/get-sinfo').then((response)=>{
      console.log(response.data);
      console.log(response)
    //   console.log(response.data.image_list[0]);
      setdata(response.data)
    }).catch((error)=>{
    //   console.log(error)
    })

    // axios.get('http://127.0.0.1:8000/api/node_groups_list').then((response)=>{
    //   console.log(response.data[0].groups);
    //   setgroups(response.data[0].groups)
    // }).catch((error)=>{
    //   console.log(error)
    // })
  }, []);
  
    const handleInputChange = (event) => {
      const { name, value } = event.target;
      setFormData({ ...formData, [name]: value });
    };

    const handleInputChange1 = (event) => {
      if (event.target.type === 'file') {
        const file = event.target.files[0];

  if (file) {
    const fileType = file.type;
    console.log(fileType)
    if (fileType == 'text/csv') {
      setFormData1({
        ...formData1,
        [event.target.name]: event.target.files[0]
      });
    } else {
      toast.error("Please select a csv file.")
    }
  }

      } else {
        setFormData1({
          ...formData1,
          [event.target.name]: event.target.value
        });
      }
    };
  
    const clearPage = (event) => {
      setFormData({
        email: '',
        ip: '',
        groups: '',
        managementIp: '',
        ManagementUsername: '',
        MacAddress: '',
        password: ''
      });
    };
  

    const handleKeyPress = (event) => {
      if (event.key === 'Enter') {
        handleSubmit1(event);
      }
    };

    const handleSubmit = async (event) => {
      event.preventDefault();

      if (formData.email == "" || formData.ip == "" || formData.groups == "" || formData.managementIp == "" || formData.ManagementUsername == "" || formData.password == "" || formData.MacAddress == "") {
        // some input fields are empty, show an error message
          console.log('Please fill all input fields.');
        toast.error("Please enter all the fields")


      } else {

                // all input fields are filled, do something
                console.log('All input fields are filled.');
                // console.log(isIP(formData.ip))
                // console.log(isIP(formData.managementIp))
                  console.log(formData);
                  try {
                    const response = await axios.post('http://127.0.0.1:8000/api/admin/node-defination', {
                      // user,
                      // password,
                      formData
                    });
                    // console.log(response);
                  } catch (error) {
                    console.error(error);
                    toast.error(error)
                  }




                  setFormData({
                    email: '',
                    ip: '',
                    groups: '',
                    managementIp: '',
                    ManagementUsername: '',
                    MacAddress: '',
                    password: '',
                  });

                  toast.success("Node added successfully");
      }

      //Setting new data

      axios.get('http://127.0.0.1:8000/api/test').then((response)=>{
        console.log(response.data);
        setdata(response.data)
      }).catch((error)=>{
        console.log(error)
      })
      

    };

    const handleSubmit1 = async (event) => {
      event.preventDefault();
      if ( formData1.multiplegroups == "" || formData1.multiplemanagementIp == "" || formData1.multipleManagementUsername == "" || formData1.multiplepassword == "" || formData1.multipleMacAddress == null) {
        // some input fields are empty, show an error message
          console.log('Please fill all input fields.');
        toast.error("Please enter all the fields")


      } else {

                // all input fields are filled, do something
                console.log('All input fields are filled.');
                // console.log(isIP(formData.ip))
                // console.log(isIP(formData.managementIp))
                  console.log(formData1);
                  

                  try {
                    const response = await axios.post('http://127.0.0.1:8000/api/admin/multiple-node-defination',formData1, {
                      headers: {
                        'Content-Type': 'multipart/form-data'
                      }
                    });
                    toast.success("Nodes added successfully");
                    if(response.data["duplicate_hostnames"].length > 0){
                      toast.warning("Duplicate hostname : " + response.data["duplicate_hostnames"])
                    }
                    if(response.data["duplicate_ips"].length > 0){
                      toast.warning("Duplicate ips : " + response.data["duplicate_ips"])
                    }
                    if(response.data["duplicate_macs"].length > 0){
                      toast.warning("Duplicate macs : " + response.data["duplicate_macs"])
                    }

                  } catch (error) {
                    console.error(error);
                    toast.error(error)
                  }

                  setFormData1({
                    multiplegroups: '',
                    multiplemanagementIp: '',
                    multipleManagementUsername: '',
                    multipleMacAddress: null,
                    multiplepassword: '',
                  });
                  fileInputRef.current.value = '';


      }
      
            //Setting new data

            axios.get('http://127.0.0.1:8000/api/test').then((response)=>{
              console.log(response.data);
              setdata(response.data)
            }).catch((error)=>{
              console.log(error)
            })

    };
  

    const downloadFile = () => {
      // Make a GET request to the Django backend endpoint that serves the file
      axios.get('http://127.0.0.1:8000/api/download-dummy-file', { responseType: 'blob' })
        .then(response => {
          // Create a URL object from the response data
          const fileUrl = URL.createObjectURL(response.data);
          // Create an anchor element
          const link = document.createElement('a');
          // Set the href and download attributes of the anchor element
          link.href = fileUrl;
          link.download = 'sample.csv'; // Specify the desired file name
          // Programmatically trigger a click event on the anchor element to start the download
          link.click();
          // Clean up by revoking the URL object
          URL.revokeObjectURL(fileUrl);
        })
        .catch(error => {
          // Handle the error
          console.error('Error downloading file:', error);
        });
    };

    const AddNode = (event) => {
        setIsAddNodeVisible(!isAddNodeVisible);
      };

    const updateData = (group) => {
      axios.get('http://127.0.0.1:8000/api/getnodelist',group).then((response)=>{
        console.log(response.data);
        setdata(response.data)
      }).catch((error)=>{
        console.log(error)
      })
      };

      const handlePropChange = (newValue) => {
        // Change the prop value
        // console.log(newValue[0][0])
        setdata(newValue);
      };

  return (
    <div className='NodeDefinationTop'>
                    <div className='AdminDashboardTop'>

                      {/* <div className="sidebar">
                      <button class="" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling">Enable body scrolling</button>

<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
  <div class="offcanvas-header">
    <h5 class="offcanvas-title" id="offcanvasScrollingLabel">Offcanvas with body scrolling</h5>
    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
  </div>
  <div class="offcanvas-body">
    <p>Try scrolling the rest of the page to see this option in action.</p>
  </div>
</div>
                      </div> */}
                {/* <Sidebar className="SidebarAdminPage"></Sidebar> */}
                <SideBar></SideBar>

                <div className="AdminDashboardContentsTop container pt-5">

                <Box sx={{ width: '100%' }}>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs value={value} onChange={handleChange} textColor='#424242' variant="scrollable" scrollButtons="auto" aria-label="scrollable auto tabs example">
          <Tab label="Parttions" {...a11yProps(0)} />
          <Tab label="Node Search" {...a11yProps(1)} />
          <Tab label="squeue" {...a11yProps(2)} />
          <Tab label="saccount" {...a11yProps(3)} />
          <Tab label="sinfo" {...a11yProps(4)} />
          <Tab label="jobs" {...a11yProps(5)} />
          <Tab label="create" {...a11yProps(6)} />
          <Tab label="search" {...a11yProps(7)} />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
      <div className="AdminNodeDefinationTable">
                        {/* <NodeListTable rows={data}></NodeListTable> */}
                        <EnhancedTable grouplist={groups}  grouplistfunction={setgroups} rows={data} onButtonClick={handlePropChange}></EnhancedTable>
                    </div>
      </TabPanel>
      <TabPanel value={value} index={1}>
        
      <div className="">
                      <div className="">
                      {/* <button className="addNodeButton button-50" data-bs-toggle="modal" data-bs-target="#addSingleImage">
                            Add Image
                        </button> */}

                        <label htmlFor="nodename" className='me-2'>Node :</label>
                        <input type="text" name='nodename' id='nodename' value={node} onChange={(e)=>{
                          setNode(e.target.value)
                        }}/>
                        <button className='ms-2' onClick={searchNode}>
                          <span className="me-2">Search</span>
                        <i class="fa-solid fa-magnifying-glass"></i>
                        </button>

                      </div>

                    </div>

                    <div className="informationOfNode p-5">
                    {NodeInfo.length > 0 ? (
        <div className='text-start'>
                {Object.entries(NodeInfo[0]).map(([key, value]) => (
        <p key={key}>
          <b>{key} : </b> {value}
        </p>
      ))}
        </div>
      ) : (
        <p>
          {/* We currently don't have information on this image. */}
        </p>
      )}
                </div>

      </TabPanel>

      <TabPanel value={value} index={2}>
        
        
      {/* <div className='workloadpageaccordian'>
          <div
            onClick={handlequeueClick}
            style={{ cursor: 'pointer' }}
            className='workloadpageaccordiancontent d-flex justify-content-around'
          >
            <div>squeue</div>
            {showqueue ? (
        <i class="fa-solid fa-chevron-up"></i>
      ) : (
        <i class="fa-solid fa-chevron-down"></i>
      )}
          </div>
          
        </div> */}
        <div className=''>
        <TableComponent columns={columns} rows={squeue}></TableComponent>
        </div>

      </TabPanel>

      <TabPanel value={value} index={3} oncl>
        
      <div className='workloadpageaccordian'>
          <TableComponent columns={saccountcolumns} rows={saccount}></TableComponent>
        </div>

      </TabPanel>
      <TabPanel value={value} index={4} oncl>
        
      <div className='workloadpageaccordian'>
          <TableComponent columns={sinfocolumns} rows={saccount}></TableComponent>
        </div>

      </TabPanel>
      <TabPanel value={value} index={5} oncl>
        
      <div className='workloadpageaccordian'>
          <TableComponent columns={jobsinfocolumns} rows={jobsinfo}></TableComponent>
        </div>

      </TabPanel>
      <TabPanel value={value} index={6} oncl>
        
      <div className='workloadpageaccordian d-flex justify-content-around'>
        
        {/* <button className='me-2'>Batch job</button>
        <button className='me-2'>Reservation</button>
        <button className='me-2'>Partition</button> */}

        <BatchJob></BatchJob>

        <Partition></Partition>

        <Reservation></Reservation>


        </div>

      </TabPanel>
      <TabPanel value={value} index={7} oncl>
        
        <div className='workloadpageaccordian'>
          
        <WorkloadSearchTab></WorkloadSearchTab>
                    </div>
  
        </TabPanel>
    </Box>
                </div>


                

            </div>

<ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
    </div>
  )
}
