import React, { useState } from 'react'
import "./DashboardCards.css"
import { useEffect } from 'react';
import axios from 'axios';

export default function DashboardCards() {

  const [totalUsers , settotalUsers] = useState();


  useEffect(() => {
    axios.get('http://127.0.0.1:8000/api/get-usercount').then((response)=>{
      console.log(response.data);
      settotalUsers(response.data)
    }).catch((error)=>{
      console.log(error)
    })
  }, []);



  return (
   
                    
              <div className="row AdminDashboardTopCardsRow" style={{margin:"auto"}}>
                
                <div class="cardMain col mx-2">
  <p>Total Users</p>
  <strong>{totalUsers}</strong>
  <p class="cardFooter">See all users</p>

                </div>
                
                <div class="cardMain col mx-2">
  <p>Total Users</p>
  <strong>50</strong>
  <p class="cardFooter">See all users</p>
</div>
               
                
                <div class="cardMain col mx-2">
  <p>Total Users</p>
  <strong>50</strong>
  <p class="cardFooter">See all users</p>
</div>
                
              
            </div>
  
  )
}
