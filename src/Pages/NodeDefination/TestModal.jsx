import React from 'react'

export default function TestModal({node,nodeInfo}) {
  return (
    <div>
          {/* <div>

    </div> */}
      {nodeInfo.length > 0 ? (
        <div className='text-start'>
          <p>
            <b>Hostname : </b> {node}
          </p>
                {Object.entries(nodeInfo[0]).map(([key, value]) => (
        <p key={key}>
          <b>{key} : </b> {value}
        </p>
      ))}
        </div>
      ) : (
        <p>
          We currently don't have information on this image.
        </p>
      )}
      {/* {nodeInfo.length > 0 ? (
        <div className='text-start'>
          <div className="row">
          <div className="col">
            <b>Node : </b>
            {nodeInfo[0][0]}
          </div>
          <div className="col">
            <b>Groups :</b>
            {nodeInfo[0][1]}
          </div>
        </div>
        <div className="row">
          <div className="col">
            <b>Status : </b>
           {nodeInfo[0][2]}
          </div>
          <div className="col">
            <b>Ip : </b>
            {nodeInfo[0][3]}
          </div>
        </div>
        <div className="row">
          <div className="col">
            <b>Os : </b>
           {nodeInfo[0][4]}
          </div>
          <div className="col">
            <b>Currstate : </b>
            {nodeInfo[0][5]}
          </div>
        </div>
        <div className="row">
          <div className="col">
            <b>Bmc : </b>
            {nodeInfo[0][6]}
          </div>
          <div className="col">
            <b>Username : </b>
          {nodeInfo[0][7]}
          </div>
        </div>
        <div className="row">
          <div className="col">
            <b>Password : </b>
          {nodeInfo[0][8]}
          </div>
          <div className="col">
            <b>Mac :  </b>
          {nodeInfo[0][9]}
          </div>
        </div>
        <div className="row">
          <div className="col">
            <b>Mgt : </b>
           {nodeInfo[0][10]}
          </div>
          <div className="col">
            <b>Serialspeed : </b>
          {nodeInfo[0][11]}
          </div>
        </div>
        <div className="row">
          <div className="col">
            <b>Netboot : </b>
          {nodeInfo[0][12]}
          </div>
          <div className="col">
            <b>Arch : </b>
           {nodeInfo[0][13]}
          </div>
        </div>
        <div className="row">
          <div className="col">
            <b>Provmethod : </b>
           {nodeInfo[0][14]}
          </div>
          <div className="col">
            <b>Profile : </b>
            {nodeInfo[0][15]}
          </div>
        </div>
        </div>
      ) : (
        <p>
          We currently don't have information on this node.
        </p>
      )} */}
                
    </div>
  )
}
