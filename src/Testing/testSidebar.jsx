import React from 'react'
import SideBar from '../Components/Sidebar/SideBar'
import TestLayout from './testLayout'
import BasicTest from './BasicTest'

export default function TestSidebar() {

  return (
    <div>
      
      <BasicTest childComponent={<TestLayout />} ></BasicTest>

    </div>
  )
}
