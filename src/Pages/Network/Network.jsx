import React from 'react'
import axios from 'axios';

export default function Network() {

  const getData = () => {
    axios.get('http://127.0.0.1:8000/api/admin/get-network-info')
    .then(response => {
      console.log(response)
    })
    .catch(error => {
      // Handle the error
      console.log(error);
    });
  }

  return (
    <div>Network
      <button onClick={getData}>Click me</button>
    </div>
  )
}
