import React from 'react'
import Sidebar from '../../Components/Sidebar_backup/Sidebar'
import DBUserTable from '../../Components/DashboardUserTable/DBUserTable'
import { useState } from 'react';
import { toast , ToastContainer} from 'react-toastify';
import {isIP, isIPv4} from 'is-ip';
// import "./NodeDefination.css"
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import NodeListTable from '../../Components/NodeListTable/NodeListTable';
import { useEffect } from 'react';
import EnhancedTable from './dummy';
import { useRef } from 'react';
import { CircularProgress, LinearProgress } from '@mui/material';
import SideBar from '../../Components/Sidebar/SideBar';

export default function ImageGroups() {

    const [isAddNodeVisible, setIsAddNodeVisible] = useState(false);
    const [isCustomNodeVisible, setIsCustomNodeVisible] = useState(false);
    const [isDeleteNodeVisible, setIsDeleteNodeVisible] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const fileInputRef = useRef(null);

    const [groups , setgroups] = useState([]);
    const [data , setdata] = useState([]);
    
    const [selectedFile, setSelectedFile] = useState("");

    const handleFileChange = (event) => {
      const file = event.target.value;
      setSelectedFile(file);
    };
  
    const handleUpload = () => {
      // You can now access the path of the selected file via selectedFile variable

      if (selectedFile) {
        console.log('File path:', selectedFile);
        setIsLoading(true);
        axios.post('http://127.0.0.1:8000/api/admin/add-image',{selectedFile}).then((response)=>{
          console.log(response)
          toast.success("Image added successfully")
          setIsLoading(false);

          axios.get('http://127.0.0.1:8000/api/admin/images').then((response)=>{
            console.log(response.data.image_list);
            console.log(response)
          //   console.log(response.data.image_list[0]);
            setdata(response.data.image_list)
          }).catch((error)=>{
            console.log(error)
          })

          }).catch((error)=>{
            console.log(error)
          })
        setSelectedFile("");
        // You can send the file path to your backend or perform any other necessary actions
      }else {
        toast.error("please enter something !")
      }
    };

    const [formData, setFormData] = useState({
      email: '',
      ip: '',
      groups: '',
      managementIp: '',
      ManagementUsername: '',
      MacAddress: '',
      password: '',
    });

    const [formData1, setFormData1] = useState({
      multiplegroups: '',
      multiplemanagementIp: '',
      multipleManagementUsername: '',
      multipleMacAddress: '',
      multiplepassword: '',
    });

    const navigate = useNavigate()

    
    
  useEffect(() => {
    axios.get('http://127.0.0.1:8000/api/admin/images').then((response)=>{
      console.log(response.data.image_list);
      console.log(response)
    //   console.log(response.data.image_list[0]);
      setdata(response.data.image_list)
    }).catch((error)=>{
    //   console.log(error)
    })

    // axios.get('http://127.0.0.1:8000/api/node_groups_list').then((response)=>{
    //   console.log(response.data[0].groups);
    //   setgroups(response.data[0].groups)
    // }).catch((error)=>{
    //   console.log(error)
    // })
  }, []);
  
    const handleInputChange = (event) => {
      const { name, value } = event.target;
      setFormData({ ...formData, [name]: value });
    };

    const handleInputChange1 = (event) => {
      if (event.target.type === 'file') {
        const file = event.target.files[0];

  if (file) {
    const fileType = file.type;
    console.log(fileType)
    if (fileType == 'text/csv') {
      setFormData1({
        ...formData1,
        [event.target.name]: event.target.files[0]
      });
    } else {
      toast.error("Please select a csv file.")
    }
  }

      } else {
        setFormData1({
          ...formData1,
          [event.target.name]: event.target.value
        });
      }
    };
  
    const clearPage = (event) => {
      setFormData({
        email: '',
        ip: '',
        groups: '',
        managementIp: '',
        ManagementUsername: '',
        MacAddress: '',
        password: ''
      });
    };
  

    const handleKeyPress = (event) => {
      if (event.key === 'Enter') {
        handleSubmit1(event);
      }
    };

    const handleSubmit = async (event) => {
      event.preventDefault();

      if (formData.email == "" || formData.ip == "" || formData.groups == "" || formData.managementIp == "" || formData.ManagementUsername == "" || formData.password == "" || formData.MacAddress == "") {
        // some input fields are empty, show an error message
          console.log('Please fill all input fields.');
        toast.error("Please enter all the fields")


      } else {

                // all input fields are filled, do something
                console.log('All input fields are filled.');
                // console.log(isIP(formData.ip))
                // console.log(isIP(formData.managementIp))
                  console.log(formData);
                  try {
                    const response = await axios.post('http://127.0.0.1:8000/api/admin/node-defination', {
                      // user,
                      // password,
                      formData
                    });
                    // console.log(response);
                  } catch (error) {
                    console.error(error);
                    toast.error(error)
                  }




                  setFormData({
                    email: '',
                    ip: '',
                    groups: '',
                    managementIp: '',
                    ManagementUsername: '',
                    MacAddress: '',
                    password: '',
                  });

                  toast.success("Node added successfully");
      }

      //Setting new data

      axios.get('http://127.0.0.1:8000/api/test').then((response)=>{
        console.log(response.data);
        setdata(response.data)
      }).catch((error)=>{
        console.log(error)
      })
      

    };

    const handleSubmit1 = async (event) => {
      event.preventDefault();
      if ( formData1.multiplegroups == "" || formData1.multiplemanagementIp == "" || formData1.multipleManagementUsername == "" || formData1.multiplepassword == "" || formData1.multipleMacAddress == null) {
        // some input fields are empty, show an error message
          console.log('Please fill all input fields.');
        toast.error("Please enter all the fields")


      } else {

                // all input fields are filled, do something
                console.log('All input fields are filled.');
                // console.log(isIP(formData.ip))
                // console.log(isIP(formData.managementIp))
                  console.log(formData1);
                  

                  try {
                    const response = await axios.post('http://127.0.0.1:8000/api/admin/multiple-node-defination',formData1, {
                      headers: {
                        'Content-Type': 'multipart/form-data'
                      }
                    });
                    toast.success("Nodes added successfully");
                    if(response.data["duplicate_hostnames"].length > 0){
                      toast.warning("Duplicate hostname : " + response.data["duplicate_hostnames"])
                    }
                    if(response.data["duplicate_ips"].length > 0){
                      toast.warning("Duplicate ips : " + response.data["duplicate_ips"])
                    }
                    if(response.data["duplicate_macs"].length > 0){
                      toast.warning("Duplicate macs : " + response.data["duplicate_macs"])
                    }

                  } catch (error) {
                    console.error(error);
                    toast.error(error)
                  }

                  setFormData1({
                    multiplegroups: '',
                    multiplemanagementIp: '',
                    multipleManagementUsername: '',
                    multipleMacAddress: null,
                    multiplepassword: '',
                  });
                  fileInputRef.current.value = '';


      }
      
            //Setting new data

            axios.get('http://127.0.0.1:8000/api/test').then((response)=>{
              console.log(response.data);
              setdata(response.data)
            }).catch((error)=>{
              console.log(error)
            })

    };
  

    const downloadFile = () => {
      // Make a GET request to the Django backend endpoint that serves the file
      axios.get('http://127.0.0.1:8000/api/download-dummy-file', { responseType: 'blob' })
        .then(response => {
          // Create a URL object from the response data
          const fileUrl = URL.createObjectURL(response.data);
          // Create an anchor element
          const link = document.createElement('a');
          // Set the href and download attributes of the anchor element
          link.href = fileUrl;
          link.download = 'sample.csv'; // Specify the desired file name
          // Programmatically trigger a click event on the anchor element to start the download
          link.click();
          // Clean up by revoking the URL object
          URL.revokeObjectURL(fileUrl);
        })
        .catch(error => {
          // Handle the error
          console.error('Error downloading file:', error);
        });
    };

    const AddNode = (event) => {
        setIsAddNodeVisible(!isAddNodeVisible);
      };

    const updateData = (group) => {
      axios.get('http://127.0.0.1:8000/api/getnodelist',group).then((response)=>{
        console.log(response.data);
        setdata(response.data)
      }).catch((error)=>{
        console.log(error)
      })
      };

      const handlePropChange = (newValue) => {
        // Change the prop value
        // console.log(newValue[0][0])
        setdata(newValue);
      };

  return (
    <div className='NodeDefinationTop'>
                    <div className='AdminDashboardTop'>

                      {/* <div className="sidebar">
                      <button class="" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling">Enable body scrolling</button>

<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
  <div class="offcanvas-header">
    <h5 class="offcanvas-title" id="offcanvasScrollingLabel">Offcanvas with body scrolling</h5>
    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
  </div>
  <div class="offcanvas-body">
    <p>Try scrolling the rest of the page to see this option in action.</p>
  </div>
</div>
                      </div> */}
                {/* <Sidebar className="SidebarAdminPage"></Sidebar> */}
                <SideBar></SideBar>

                <div className="AdminDashboardContentsTop container">
{/* 
                    <div className="AdminNodeDefinationTable">
                        <NodeListTable rows={data}></NodeListTable>
                    </div> */}




{/* list elements */}

{/* end list */}

                    <div className="AdminNodeDefinationTable mt-5">
                        {/* <NodeListTable rows={data}></NodeListTable> */}
                        <EnhancedTable grouplist={groups}  grouplistfunction={setgroups} rows={data} onButtonClick={handlePropChange}></EnhancedTable>
                    </div>

                    <div className="row mt-5">
                      <div className="col">
                      <button className="addNodeButton button-50" data-bs-toggle="modal" data-bs-target="#addSingleImage">
                            Add Image
                        </button>

                      </div>

                    </div>
                </div>


{/* <!-- Modal --> */}
<div class="modal fade" id="addSingleImage" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Add image</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       
      <form>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Image</label>
    <input type="text" class="form-control"  name="email" value={selectedFile} onChange={handleFileChange}></input>
 
  </div>

</form>

<div>
{isLoading && <div><CircularProgress></CircularProgress></div>}
</div>

<div className='my-5'>
{isLoading && <div><LinearProgress></LinearProgress></div>}
</div>


      </div>
      <div class="modal-footer">
        <button type="button" class="NodeDefinationButtons button-55" data-bs-dismiss="modal">Close</button>
        <button type="button" class="NodeDefinationButtons button-55" onClick={handleUpload}>Add</button>
      </div>
    </div>
  </div>
</div>

            </div>

<ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
    </div>
  )
}
