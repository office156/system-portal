import React, { useState } from 'react'
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Typography } from '@mui/material';
import axios from 'axios';
import { ToastContainer ,toast } from 'react-toastify';
import TableComponent from '../TableComponent/TableComponent';

export default function WorkloadSearchTab() {

  const [jobId,setJobId] = useState("")
  const [jobIdSearch,setJobIdSearch] = useState([])
  const [specificUserJob , setSpecificUserJob] = useState("")
  const [specificUserJobData , setSpecificUserJobData] = useState([])
  const [JobsInspecificState , setJobsInspecificState] = useState("")
  const [SlurmPname , setSlurmPname] = useState("")
  const [SlurmPstate , setSlurmPstate] = useState("")
  const [ReservName , setReservName] = useState("")


  const specificUserJobColumns = [
    { field: 'jobid', headerName: 'Job ID' },
    { field: 'partition', headerName: 'Partition' },
    { field: 'name', headerName: 'Name' },
    {
      field: 'user',
      headerName: 'User'
    },
    { field: 'st', headerst: 'ST' },
    { field: 'time', headertime: 'Time' },
    { field: 'nodes', headerName: 'Nodes' },
    { field: 'nodelist', headerName: 'Nodelist' },
  ];

   function searchJob(jobId) {
    console.log("You searched for : " + jobId)
    const url = "http://127.0.0.1:8000/api/get-jobidData/" + jobId 
    axios.get(url).then((response)=>{
      console.log(response.data.length)
      
      if(response.data.length == 0){
        toast.error("This job id doesn't exist !")
      }else{
        setJobIdSearch(response.data)
      }

      console.log(jobIdSearch[0]["jobid"])

      // setNodeInfo(response.data)
      // setselectedNode(node)
    }).catch((error)=>{
      console.log(error)
    })
    setJobId('')
  }


   function searchspecificUserJob(specificUserJob) {
    console.log("You searched for : " + specificUserJob)
    const url = "http://127.0.0.1:8000/api/get-userjobdata/" + specificUserJob 
    axios.get(url).then((response)=>{
      console.log(response.data)
      
      if(response.data.length == 0){
        toast.error("This user doesn't exist !")
      }else{
        setSpecificUserJobData(response.data)
      }

    }).catch((error)=>{
      console.log(error)
    })
    setSpecificUserJob('')
  }
   
  function searchJobsInspecificState(JobsInspecificState) {
    console.log("You searched for : " + JobsInspecificState)
  }
   
  function searchSlurmPname(SlurmPname) {
    console.log("You searched for : " + SlurmPname)
  }
   
  function searchSlurmPstate(SlurmPstate) {
    console.log("You searched for : " + SlurmPstate)
  }
   
  function searchReservName(ReservName) {
    console.log("You searched for : " + ReservName)
  }

  return (
    <div>
        <div>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Job Id</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div className="accordianContentSearch d-flex justify-content-around">
            <input type="number" placeholder="Please enter something" value={jobId} onChange={e => setJobId(e.target.value)} className="w-100 me-5" />
            <button className='ms-5 ps-3 pe-3 d-flex' onClick={()=>{
              searchJob(jobId)
            }}>
              <span className='pe-3'><i class="fa-brands fa-searchengin"></i></span>
              Search</button>

              
          </div>
        </AccordionDetails>
        <div className="jobIdSearch">
              {/* {} */}
              {jobIdSearch.length > 0 && <div className='border py-3'>

                <strong className=''>Job Name : </strong>
                <span className='me-2 text-start'>{jobIdSearch[0]["jobname"]}</span>
                
                <strong className='ms-5'>Partition : </strong>
                <span className='me-2 text-start'>{jobIdSearch[0]["partition"]}</span>
                <br />
                <strong className=''>account : </strong>
                <span className='me-2 text-start'>{jobIdSearch[0]["account"]}</span>
                
                <strong className='ms-5'>allocatedcpu : </strong>
                <span className='me-2 text-start'>{jobIdSearch[0]["alloccpus"]}</span>
                <br />
                <strong className=''>state : </strong>
                <span className='me-2 text-start'>{jobIdSearch[0]["state"]}</span>
                <strong className='ms-5'>state : </strong>
                <span className='me-2 text-start'>{jobIdSearch[0]["state"]}</span>

                </div>}
              </div>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Specific User's job(s)</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div className="accordianContentSearch d-flex justify-content-around">
            <input type="text" placeholder="Please enter something" value={specificUserJob} onChange={e => setSpecificUserJob(e.target.value)} className="w-100 me-5" />
            <button className='ms-5 ps-3 pe-3 d-flex' onClick={()=>{
              searchspecificUserJob(specificUserJob)
            }}>
              <span className='pe-3'><i class="fa-brands fa-searchengin"></i></span>
              Search</button>
          </div>
        </AccordionDetails>
        {specificUserJobData.length > 0 && <div className='py-3'><TableComponent columns={specificUserJobColumns} rows={specificUserJobData}></TableComponent></div>}
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Job(s) in a Specific State</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div className="accordianContentSearch d-flex justify-content-around">
            <input type="text" placeholder="Please enter something" onChange={e => setJobsInspecificState(e.target.value)} className="w-100 me-5" />
            <button className='ms-5 ps-3 pe-3 d-flex'>
              <span className='pe-3'><i class="fa-brands fa-searchengin"></i></span>
              Search</button>
          </div>
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Slurm Partition Name</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div className="accordianContentSearch d-flex justify-content-around">
            <input type="text" placeholder="Please enter something" onChange={e => setSlurmPname(e.target.value)} className="w-100 me-5" />
            <button className='ms-5 ps-3 pe-3 d-flex'>
              <span className='pe-3'><i class="fa-brands fa-searchengin"></i></span>
              Search</button>
          </div>
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Slurm Partition State</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div className="accordianContentSearch d-flex justify-content-around">
            <input type="text" placeholder="Please enter something" onChange={e => setSlurmPstate(e.target.value)} className="w-100 me-5" />
            <button className='ms-5 ps-3 pe-3 d-flex'>
              <span className='pe-3'><i class="fa-brands fa-searchengin"></i></span>
              Search</button>
          </div>
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Reservation Name</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div className="accordianContentSearch d-flex justify-content-around">
            <input type="text" placeholder="Please enter something" onChange={e => setReservName(e.target.value)} className="w-100 me-5" />
            <button className='ms-5 ps-3 pe-3 d-flex'>
              <span className='pe-3'><i class="fa-brands fa-searchengin"></i></span>
              Search</button>
          </div>
        </AccordionDetails>
      </Accordion>
      
    </div>


    <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
    </div>
  )
}
