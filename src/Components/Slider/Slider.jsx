import React from 'react'
import { Slider } from '@mui/material'

export default function CustomSlider() {
  return (
    <div className='SliderTop'>
        <div style={{width: "250px"}}>
      <Slider
        valueLabelDisplay="auto"
        aria-label="pretto slider"
        defaultValue={20}
        color="success"
      />
 </div>
    </div>
  )
}
