import React from 'react'

export default function TestLayout() {
  return (
    <div>
                <div class="container text-center">
            <div class="row">
              <div class="col bg-primary border">
                1 of 2
              </div>
              <div class="col bg-primary border">
                2 of 2
              </div>
            </div>
            <div class="row">
              <div class="col bg-primary border">
                1 of 3
              </div>
              <div class="col bg-primary border">
                2 of 3
              </div>
              <div class="col bg-primary border">
                3 of 3
              </div>
            </div>
          </div>
  </div>
  )
}
