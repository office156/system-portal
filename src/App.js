import logo from './logo.svg';
import './App.css';
import Sidebar from './Components/Sidebar_backup/Sidebar';
import AdminDashboard from './Pages/AdminDashboard/AdminDashboard';
import { BrowserRouter as Router, Switch, Route, Routes } from 'react-router-dom';
import Page1 from './Pages/Page1/Page1';
import Page2 from './Pages/Page2/Page2';
import LandingPage from './Pages/LandingPage/LandingPage';
import Login from './Pages/Login/Login';
import AddUser from './Pages/AdminAddUser/AddUser';
// import BasicTest from './Testing/BasicTest';
import MultiPageForm from './Testing/BasicTest';
import MultiPageUserForm from './Components/MultiPageUserForm/MultiPageUserForm';
import NodeDefination from './Pages/NodeDefination/NodeDefination';
import BasicTest from './Testing/BasicTest';
import ImageGroups from './Pages/ImageGroups/ImageGroups';
import Network from './Pages/Network/Network';
import WorkloadManager from './Pages/WorkloadManager/WorkloadManager';
import BasicTabs from './Pages/WorkloadManager/Test/BasicTabs';
import RootAccount from './Pages/RootAccount/RootAccount';
import TestSidebar from './Testing/testSidebar';

function App() {
  return (
<Router>
    <div className="App">
      <Routes>
                 <Route exact path='/test' element={< TestSidebar />}></Route>
                 <Route exact path='/admin-dashboard' element={< AdminDashboard />}></Route>
                 <Route exact path='/root-account' element={< RootAccount />}></Route>
                 <Route exact path='/system' element={< Page1 />}></Route>
                 <Route exact path='/network-groups' element={< Page2 />}></Route>
                 <Route exact path='/' element={< LandingPage />}></Route>
                 <Route exact path='/login' element={< Login />}></Route>
                 {/* <Route exact path='/test' element={< MultiPageForm />}></Route> */}
                 <Route exact path='/new-user' element={<><AddUser /></>}></Route>
                 <Route exact path='/node-defination' element={<><NodeDefination /></>}></Route>
                 <Route exact path='/image-groups' element={<><ImageGroups /></>}></Route>
                 <Route exact path='/network' element={<><Network /></>}></Route>
                 <Route exact path='/workload' element={<><WorkloadManager /></>}></Route>
                 {/* <Route exact path='/home' element={< Home />}></Route>
                 <Route exact path='/about' element={< AboutUs />}></Route>
                 <Route exact path='/contact' element={< ContactUs />}></Route>
                 <Route exact path="/registration" element={<><Navbar/><Final_form/></>}/>
                 <Route exact path="/bulk-registration" element={<><Navbar/><BulkRegistration/></>}/>
                 <Route exact path="/login" element={<Login/>}/>
                 <Route exact path="/done" element={<RegDone/>}/>
                 <Route exact path="/admin" element={<AdminPanel/>}/>
                 <Route exact path="/admin-users" element={<UserList/>}/>
                 <Route exact path="/my-courses" element={<MyCourses/>}/>
                 <Route exact path="/courses" element={<><Navbar /><CourseBasic/><Footer /></>}/>
                 <Route exact path="/learning-path" element={<><Navbar /><LearningPathHpc/> <Footer /></>}/> */}
          </Routes>
    </div>
    </Router>
  );
}

export default App;
